<?php

namespace App\Http\Controllers;

class TablesController extends Controller
{
    public function table()
    {
        return view('tables', [
            'title' => 'Tables',
            'name' => 'Tables',
        ]);
    }
}
