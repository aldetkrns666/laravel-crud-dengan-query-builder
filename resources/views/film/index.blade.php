@extends('layout.master')
@section('content')
    
    <table id="example1" class="table table table-bordered table-striped">
        <thead>
            <tr>
                <td style="text-align: center; font-weight:bold">No</td>
                <td style="text-align: center; font-weight:bold">Judul</td>
                <td style="text-align: center; font-weight:bold">Genre</td>
                <td style="text-align: center; font-weight:bold">Ringkasan</td>
                <td style="text-align: center; font-weight:bold">Tahun</td>
                <td style="text-align: center; font-weight:bold">Poster</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($movie as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$value->judul }}</td>
                <td>{{$value->genre_id}}</td>
                <td>{{$value->ringkasan }}</td>
                <td>{{$value->tahun }}</td>
                <td>{{$value->poster }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Data </td>
            </tr>
            @endforelse
        </tbody>
    </table>
@endsection